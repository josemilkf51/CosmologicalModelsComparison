# Cosmological models comparison using python
## Description
This project is part of the cosmology subject from the Fundamental Physics Bachelor (Universidad Complutense de Madrid). The objective of the project is to compare two cosmological models: the Einstein-de Sitter model and the Standard Cosmological model. To do this, the creation of a powerfull python script is needed. The script will hold all the calculations for the physical measures and the statistical analysis (Chi squared test), as well as the creation of graphs.
## Paper's Language
The project "paper" is entirely in Spanish. Sadly, it would take me too much time to translate this work, as I no longer have the files to edit it. Should the reader want the text translated, I encourage them to use any online translation tool such as DeepL.
