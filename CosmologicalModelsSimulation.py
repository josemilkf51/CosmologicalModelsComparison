import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad
import time


# Exercise 1 -----------------------------------------------------------------------------------------------------------
# Python implementation of the luminosity distance
def dl_esc(z, OmegaM, OmegaK, omega):  # Luminosity distance function. Defined as a scalar operation
    def f(z):
        def g(y):
            denom = (1 + y) * np.sqrt(OmegaK + OmegaM * (1 + y) + (1 - OmegaM - OmegaK) * (1 + y) ** (1 + 3 * omega))
            out = 1/denom
            return out
        salida = quad(lambda y: g(y), 0, z)[0]
        return salida

    raiz = np.sqrt(abs(OmegaK))

    if OmegaK < 0:
        salida = np.sin(raiz*f(z))/raiz
    elif OmegaK == 0:
        salida = f(z)
    else:
        salida = np.sinh(raiz*f(z))/raiz
    return salida*(1+z)

dl = np.vectorize(dl_esc) # Now the luminosity distance is vectorised with this numpy function


# Exercise 2 -----------------------------------------------------------------------------------------------------------
# Distance graphic comparison between different Omega_K values by redshift amount
N = 10
Z = np.linspace(0, 1.5, N)
d_L1 = dl(Z, 0.32, 0, -1)
d_L2 = dl(Z, 0.32, -0.5, -1)
d_L3 = dl(Z, 0.32, 0.5, -1)

plt.plot(Z, d_L1, label="Omega_K=0")
plt.plot(Z, d_L2, label="Omega_K=-0.5")
plt.plot(Z, d_L3, label="Omega_K=+0.5")
plt.xlabel("Redshift z")
plt.ylabel("Luminosity distance d_L")
plt.title("Distance by redshift given some values of Omega_K")
plt.legend()

plt.show()

print("The value of d_L(0.5,0.32,0,-1) is ", d_L1[Z==0.5])


# Exercise 3 -----------------------------------------------------------------------------------------------------------
# This exercise will use the observational data
z_obs, mu_obs, errmu_obs = np.loadtxt("Pantheon.txt", skiprows=1, usecols=(1, 4, 5), unpack=True)
# Bellow will be defined the function that minimises M tilda

def Mm(OmegaM, OmegaK, omega):
    m_i = (mu_obs - 5 * np.log10(dl(z_obs, OmegaM, OmegaK, omega))) / errmu_obs ** 2
    m = sum(m_i)
    sigma = sum(1 / errmu_obs ** 2)
    salida = m / sigma
    return salida


# Exercise 4 -----------------------------------------------------------------------------------------------------------
# Create a function that computes the Chi squared test
def Chi2_esc(OmegaM, OmegaK, omega):
    chi2i = ((mu_obs - 5 * np.log10(dl(z_obs, OmegaM, OmegaK, omega)) - Mm(OmegaM, OmegaK, omega))/errmu_obs)** 2
    salida = sum(chi2i)
    return salida

Chi2 = np.vectorize(Chi2_esc) # Vectorise the function

print("chi^2(0.32, 0, -1) = ", Chi2(0.32, 0, -1))


# Exercise 5 -----------------------------------------------------------------------------------------------------------
# Now it is possible to compute the value of Omega_M that best fits the experimental data
N = 50
omm_viables = np.linspace(0.25, 0.33, N) # Viable range of Omega_M
chi2_omm = Chi2(omm_viables, 0, -1) # Chi squared of that range
omm_min = omm_viables[np.argmin(chi2_omm)] # Best Omega_M

print("The value that fits best the experimental data is Omega_M = ", omm_min,
      "with a Chi squared score of ", min(chi2_omm))

plt.plot(omm_viables, chi2_omm, label="Chi2 of Omega_M")
chimin = (1 + min(chi2_omm)) * np.ones(N) # One sigma error (only for plot purposes)
plt.plot(omm_viables, chimin, linestyle="dashed", color="gray", label="Errores a 1 sigma")
plt.legend()
plt.show()


# Exercise 6 -----------------------------------------------------------------------------------------------------------
# Left and right Omega_M values deviated one sigma

tol = 0.2 # Set tolerance to stop the approximation
j = 0
while abs(chimin[j] - chi2_omm[j]) > tol:
    j += 1
sigma_izq = omm_viables[j]
print("The 1 sigma left value is ", sigma_izq)
j += 1
while abs(chimin[j] - chi2_omm[j]) > tol:
    j += 1
sigma_der = omm_viables[j]
print("The 1 sigma right value is ", sigma_der)



# Exercise 7 -----------------------------------------------------------------------------------------------------------
# The universe proposed by Einstein-de Chitter has OmegaM=1 and thus, OmegaK and OmegaLambda=0
EdS = Chi2(1, 0, -1) # Chi squared calculation for this cosmoligical model

print("The Chi squared value for a universe of Einstein-de Sitter, which has OmegaM=1, OmegaLambda=0 and OmegaM=0, with omega=-1 is ", EdS, ",")
print("while for the model that fits the observational data the best (LambdaCDM), it is ", min(chi2_omm))

Z = np.linspace(0.01, 2.26, len(z_obs))

# Best fit:
mejor_mu = 5 * np.log10(dl(Z, omm_min, 0,-1)) + Mm(omm_min, 0, -1)

# E-dS:
EdS_mu = 5 * np.log10(dl(Z, 1, 0,-1)) + Mm(1,0, -1)

# Plot a comparison between models and experimental data
plt.figure()
plt.plot(Z, EdS_mu, label="Universe of Einstein-de Sitter")
plt.plot(Z, mejor_mu, label="Standard Cosmological Universe")
plt.errorbar(z_obs, mu_obs, yerr=errmu_obs, fmt='.', label="Experimental data")
plt.xlabel("Redshift z")
plt.ylabel("Módulo de la distancia mu")
plt.title("Comparación entre modelos cosmológicos")
plt.legend()
plt.savefig("/home/josemi/PycharmProjects/Uni/EdS_vs_LCDM")
plt.show()

# Model separation by sigma:
print("The separation between models given in terms of sigmas is: ", abs(EdS - min(chi2_omm)) ** 0.5, " sigmas")


# Exercise 8 -----------------------------------------------------------------------------------------------------------
# Some comparisons between measures and the better model
print("Omega_M best fit model deviation from Planck 2018 value: ", abs(omm_min-0.315)/0.0128)
d_lum = dl(1.5, omm_min, 0,-1) * 4285
d_com = d_lum / (1+1.5)  # Dado que consideramos un espacio plano d_com/(1+z)=d_L/(1+z)**2
print("The luminosity distance to a Supernova with z=1.5 is ", d_lum, " Mpc in the model LambdaCDM")
print("Meanwhile, the comoving distance is ", d_com, " Mpc.")


# Exercise 9 -----------------------------------------------------------------------------------------------------------
# Plot of the two parameter Chi squared test
# It is used to approximate Omega_K and omega
N=40 # Originally 100 steps. Reduced to 40 for faster runtime
OmM = 0.315 # Set the measured Omega_M value
OmK = np.linspace(-1,0.4,N)    # Viable Omega_K values
ome = np.linspace(-3.1, -0.5, N) # Viable omega values

# Mesh creation
OmM_malla = 0.315*np.ones([N,N])
Omk_malla, ome_malla = np.meshgrid(OmK, ome)

# Chi squared calculations
t1 = time.perf_counter()
chi2_malla = list(map(Chi2,OmM_malla,Omk_malla,ome_malla))
#for i in tqdm(range(0, N)):
#    chi2_malla[i,:] = Chi2(OmM, OmK, ome[i])
t2 = time.perf_counter()
print("It took: ",t2-t1,"s to compute the chi squared test") # Calculation time

# Finally, plot the results
levels = [np.nanmin(chi2_malla), np.nanmin(chi2_malla) + 2.3, np.nanmin(chi2_malla) + 6.18, np.nanmin(chi2_malla) + 11.83]
plt.contour(Omk_malla, ome_malla, chi2_malla, levels, colors='gray')
plt.contourf(Omk_malla, ome_malla, chi2_malla, levels)
plt.title("Chi^2(OmegaM,OmegaK, omega)")
plt.xlabel("OmegaK")
plt.ylabel("omega")
plt.colorbar(plt.contourf(Omk_malla,ome_malla,chi2_malla,levels))
plt.show()
plt.savefig("/home/josemi/PycharmProjects/Uni/Contourplot")

# Minimum calculation
minimum = np.unravel_index(np.nanargmin(chi2_malla, axis=None), [N,N])
print("The minimum Chi^2 is ",np.nanmin(chi2_malla)," which corresponds to the pair (OmegaK,omega)=","(",OmK[minimum[1]],",",ome[minimum[0]],").")


# Estimated left and right values using the graph
print("The left values for the pair (OmegaK,omega) at 1 sigma are: (-0.608,-0.754)")
print("and the right values at 1 sigma are: (0.2868,-2.1459).")